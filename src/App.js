import React, { Component } from "react";
import {
  MDBNavbar,
  MDBNavbarBrand,
  MDBNavbarNav,
  MDBNavbarToggler,
  MDBCollapse,
  MDBNavItem,
  MDBFooter,
  MDBNavLink,
  MDBTooltip,
  MDBIcon,
  MDBFormInline,
} from "mdbreact";
import { HashRouter, BrowserRouter as Router, Link } from "react-router-dom";

import Routes from "./Routes";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
class App extends Component {
  // state = {
  //   collapseID: "",
  // };

  // toggleCollapse = (collapseID) => () =>
  //   this.setState((prevState) => ({
  //     collapseID: prevState.collapseID !== collapseID ? collapseID : "",
  //   }));

  // closeCollapse = (collID) => () => {
  //   const { collapseID } = this.state;
  //   window.scrollTo(0, 0);
  //   collapseID === collID && this.setState({ collapseID: "" });
  // };

  render() {
    let Button = (
      <Link to="/dashboard">
        <button className="ring-2 lg:p-2 xs:p-1 text-white uppercase ring-yellow-500 hover:bg-yellow-700 rounded lg:w-30 xs:w-15 lg:text-lg xs:text-xs">
          {localStorage.getItem("nama_dinas")}
        </button>
      </Link>
    );
    if (!localStorage.getItem("token")) {
      Button = (
        <Link to="/login">
          <button className="ring-2 lg:p-2 xs:p-1 text-white uppercase ring-yellow-500 hover:bg-yellow-700 rounded lg:w-20 xs:w-15 lg:text-lg xs:text-sm">
            Login
          </button>
        </Link>
      );
    }

    // const overlay = (
    //   <div
    //     id="sidenav-overlay"
    //     style={{ backgroundColor: "transparent" }}
    //     onClick={this.toggleCollapse("mainNavbarCollapse")}
    //   />
    // );

    // const { collapseID } = this.state;

    return (
      <HashRouter>
        <div className="flyout">
          <div className="h-full">
            <Routes />
            <ToastContainer />
          </div>
          <MDBFooter color="indigo">
            <p className="footer-copyright mb-0 py-3 text-center">
              &copy; {new Date().getFullYear()} Copyright:
              <a href="https://kotamobagukota.go.id">
                Pemerintah Kota Kotamobagu
              </a>
            </p>
          </MDBFooter>
        </div>
      </HashRouter>
    );
  }
}

export default App;
