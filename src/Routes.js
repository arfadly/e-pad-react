import React from "react";
import { Route, Switch } from "react-router-dom";

//dashboard
import Dashboard from "./pages/dashboard/dashboard";
import JenisPAD from "./pages/dashboard/JenisPAD";
import Target from "./pages/dashboard/Target";
import Realisasi from "./pages/dashboard/Realisasi";
import Bulanan from "./pages/dashboard/RealisasiDetail";
import DetilBulanan from "./pages/dashboard/DetilBulanan";
//home
import HomePage from "./pages/home/home.js";
import Login from "./pages/login/login.js";
// PRO-END

class Routes extends React.Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/dashboard" component={Dashboard} />
        <Route exact path="/target" component={Target} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/jenisPAD" component={JenisPAD} />
        <Route exact path="/realisasi" component={Realisasi} />
        <Route exact path="/realisasi/bulanan" component={Bulanan} />
        <Route
          exact
          path="/realisasi/bulanan/detail"
          component={DetilBulanan}
        />
        <Route
          render={function () {
            return <h1>Not Found</h1>;
          }}
        />
      </Switch>
    );
  }
}

export default Routes;
