import React from "react";
import {
  MDBEdgeHeader,
  MDBFreeBird,
  MDBContainer,
  MDBCol,
  MDBRow,
  MDBCardBody,
  MDBIcon,
  MDBCard,
  MDBCardTitle,
  MDBCardImage,
  MDBCardText,
  MDBAnimation,
  MDBNavLink,
  MDBBtn,
  MDBProgress,
  MDBTypography,
  MDBView,
  MDBSpinner,
  MDBTable,
  MDBTableBody,
  MDBTableHead,
} from "mdbreact";
import { Pie, Bar, Line } from "react-chartjs-2";
import "./HomePage.css";
import TopNavbar from "../../components/topNavbar";
class HomePage extends React.Component {
  state = {
    dataBar: {
      labels: [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "Mei",
        "Jun",
        "Jul",
        "Agu",
        "Sep",
        "Okt",
        "Nov",
        "Des",
      ],
      datasets: [
        {
          label: "Target",
          data: [12, 19, 3, 5, 2, 3],
          backgroundColor: [
            "rgba(255, 134, 159, 1)",
            "rgba(255, 134, 159, 1)",
            "rgba(255, 134, 159, 1)",
            "rgba(255, 134, 159, 1)",
            "rgba(255, 134, 159, 1)",
            "rgba(255, 134, 159, 1)",
            "rgba(255, 134, 159, 1)",
            "rgba(255, 134, 159, 1)",
            "rgba(255, 134, 159, 1)",
            "rgba(255, 134, 159, 1)",
            "rgba(255, 134, 159, 1)",
            "rgba(255, 134, 159, 1)",
          ],
          borderWidth: 2,
          borderColor: [
            "rgba(255, 134, 159, 1)",
            "rgba(255, 134, 159, 1)",
            "rgba(255, 134, 159, 1)",
            "rgba(255, 134, 159, 1)",
            "rgba(255, 134, 159, 1)",
            "rgba(255, 134, 159, 1)",
            "rgba(255, 134, 159, 1)",
            "rgba(255, 134, 159, 1)",
            "rgba(255, 134, 159, 1)",
            "rgba(255, 134, 159, 1)",
            "rgba(255, 134, 159, 1)",
            "rgba(255, 134, 159, 1)",
          ],
        },
        {
          label: "Realisasi",
          data: [12, 19, 3, 5, 2, 3],
          backgroundColor: [
            "rgba(104, 109, 224,1.0)",
            "rgba(104, 109, 224,1.0)",
            "rgba(104, 109, 224,1.0)",
            "rgba(104, 109, 224,1.0)",
            "rgba(104, 109, 224,1.0)",
            "rgba(104, 109, 224,1.0)",
            "rgba(104, 109, 224,1.0)",
            "rgba(104, 109, 224,1.0)",
            "rgba(104, 109, 224,1.0)",
            "rgba(104, 109, 224,1.0)",
            "rgba(104, 109, 224,1.0)",
            "rgba(104, 109, 224,1.0)",
          ],
          borderWidth: 2,
          borderColor: [
            "rgba(104, 109, 224,1.0)",
            "rgba(104, 109, 224,1.0)",
            "rgba(104, 109, 224,1.0)",
            "rgba(104, 109, 224,1.0)",
            "rgba(104, 109, 224,1.0)",
            "rgba(104, 109, 224,1.0)",
            "rgba(104, 109, 224,1.0)",
            "rgba(104, 109, 224,1.0)",
            "rgba(104, 109, 224,1.0)",
            "rgba(104, 109, 224,1.0)",
            "rgba(104, 109, 224,1.0)",
            "rgba(104, 109, 224,1.0)",
          ],
        },
      ],
    },
    barChartOptions: {
      responsive: true,
      maintainAspectRatio: true,
      scales: {
        xAxes: [
          {
            barPercentage: 1,
            gridLines: {
              display: true,
              color: "rgba(0, 0, 0, 0.1)",
            },
          },
        ],
        yAxes: [
          {
            gridLines: {
              display: true,
              color: "rgba(255, 177, 101, 1)",
            },
            ticks: {
              beginAtZero: true,
            },
          },
        ],
      },
    },
    dataLine: {
      labels: ["2017", "2018", "2019", "2020", "2021"],
      datasets: [
        {
          label: "target",
          fill: true,
          lineTension: 0.3,
          backgroundColor: "rgba(235, 77, 75,0.5)",
          borderColor: "rgba(235, 77, 75,1.0)",
          borderCapStyle: "butt",
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: "miter",
          pointBorderColor: "rgb(205, 130,1 58)",
          pointBackgroundColor: "rgb(255, 255, 255)",
          pointBorderWidth: 10,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: "rgb(0, 0, 0)",
          pointHoverBorderColor: "rgba(220, 220, 220,1)",
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: [1, 2, 3, 4, 5, 6, 7],
        },
        {
          label: "realisasi",
          fill: true,
          lineTension: 0.3,
          backgroundColor: "rgba(104, 109, 224,1.0)",
          borderColor: "rgba(104, 109, 224,1.0)",
          borderCapStyle: "butt",
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: "miter",
          pointBorderColor: "rgb(35, 26, 136)",
          pointBackgroundColor: "rgb(255, 255, 255)",
          pointBorderWidth: 10,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: "rgb(0, 0, 0)",
          pointHoverBorderColor: "rgba(220, 220, 220, 1)",
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: [2, 3, 4, 5, 6, 7, 8],
        },
      ],
    },

    lineOption: {
      tooltips: {
        callbacks: {
          label: function (tooltipItems, data) {
            return (
              "Rp " +
              data.datasets[0].data[tooltipItems.index].toLocaleString("id-ID")
            );
          },
        },
      },

      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
              callback: function (value, index, values) {
                return value.toLocaleString("id-ID");
              },
            },
          },
        ],
        xAxes: [
          {
            ticks: {
              beginAtZero: true,
              callback: function (value, index, values) {
                return value.toLocaleString("id-ID");
              },
            },
          },
        ],
      },
    },
  };
  scrollToTop = () => window.scrollTo(0, 0);
  render() {
    let random1 = Math.floor(Math.random() * Math.floor(100));
    let random2 = Math.floor(Math.random() * Math.floor(100));
    let random3 = Math.floor(Math.random() * Math.floor(100));
    let random4 = Math.floor(Math.random() * Math.floor(100));
    let colorProgress1 =
      random1 > 35 && random1 < 70
        ? "warning"
        : random1 > 70
        ? "success"
        : "danger";
    let colorProgress2 =
      random2 > 35 && random2 < 70
        ? "warning"
        : random2 > 70
        ? "success"
        : "danger";
    let colorProgress3 =
      random3 > 35 && random3 < 70
        ? "warning"
        : random3 > 70
        ? "success"
        : "danger";
    let colorProgress4 =
      random4 > 35 && random4 < 70
        ? "warning"
        : random4 > 70
        ? "success"
        : "danger";

    return (
      <>
        <TopNavbar />
        <div className="mb-20" />
        <MDBEdgeHeader color="indigo darken-3" className="sectionPage" />
        <div style={{ marginTop: "-200px" }}>
          <MDBContainer fluid>
            <MDBRow>
              <MDBCol lg="3" sm="6" xs="6">
                <MDBAnimation type="zoomIn" duration="500ms">
                  <MDBCard narrow>
                    <MDBCardBody cascade className="text-center">
                      <h6 className="h6-responsive">TOTAL PAD TAHUN 2021</h6>
                      <MDBTypography tag="h5" variant="h6-responsive">
                        {random1}%
                      </MDBTypography>
                      <MDBProgress
                        material
                        value={random1}
                        striped
                        color={colorProgress1}
                      />
                      <MDBTypography tag="h5" variant="h5-responsive">
                        Rp. 3.546.332.622,00
                      </MDBTypography>
                    </MDBCardBody>
                  </MDBCard>
                </MDBAnimation>
              </MDBCol>

              <MDBCol lg="3" sm="6" xs="6">
                <MDBAnimation type="zoomIn" duration="800ms">
                  <MDBCard narrow>
                    <MDBCardBody cascade className="text-center">
                      <h6 className="h6-responsive">TOTAL PAD BULAN INI</h6>
                      <MDBTypography tag="h5" variant="h6-responsive">
                        {random2}%
                      </MDBTypography>
                      <MDBProgress
                        material
                        value={random2}
                        striped
                        color={colorProgress2}
                      />
                      <MDBTypography tag="h5" variant="h5-responsive">
                        Rp. 7.546.332.622,00
                      </MDBTypography>
                    </MDBCardBody>
                  </MDBCard>
                </MDBAnimation>
              </MDBCol>
              <MDBCol lg="3" sm="6" xs="6">
                <MDBAnimation type="zoomIn" duration="800ms">
                  <MDBCard narrow>
                    <MDBCardBody cascade className="text-center">
                      <h6 className="h6-responsive">TOTAL PAD BULAN LALU</h6>
                      <MDBTypography tag="h5" variant="h6-responsive">
                        {random3}%
                      </MDBTypography>
                      <MDBProgress
                        material
                        value={random3}
                        striped
                        color={colorProgress3}
                      />
                      <MDBTypography tag="h5" variant="h5-responsive">
                        Rp. 8.546.332.622,00
                      </MDBTypography>
                    </MDBCardBody>
                  </MDBCard>
                </MDBAnimation>
              </MDBCol>
              <MDBCol lg="3" sm="6" xs="6">
                <MDBAnimation type="zoomIn" duration="800ms">
                  <MDBCard narrow>
                    <MDBCardBody cascade className="text-center">
                      <h6 className="h6-responsive">
                        TOTAL JENIS PAJAK TAHUN 2021
                      </h6>
                      <MDBTypography tag="h5" variant="h6-responsive">
                        {random4}%
                      </MDBTypography>
                      <MDBProgress
                        material
                        value={random4}
                        striped
                        color={colorProgress4}
                      />
                      <MDBTypography tag="h5" variant="h5-responsive">
                        Rp. 9.546.332.622,00
                      </MDBTypography>
                    </MDBCardBody>
                  </MDBCard>
                </MDBAnimation>
              </MDBCol>
            </MDBRow>
          </MDBContainer>
          <MDBContainer fluid>
            <MDBRow>
              <MDBCol lg="6">
                <h3 className="mt-12 text-center lg:text-xl xs:text-xs text-blue-800 font-bold">
                  Bar chart
                </h3>
                <Bar
                  data={this.state.dataBar}
                  options={this.state.lineOption}
                />
              </MDBCol>
              <MDBCol lg="6">
                <h3 className="mt-12 text-center lg:text-xl xs:text-xs text-blue-800 font-bold">
                  Line chart
                </h3>
                <Line
                  data={this.state.dataLine}
                  options={this.state.lineOption}
                />
              </MDBCol>
            </MDBRow>
          </MDBContainer>
          <MDBContainer fluid>
            <h4 className="mt-5 indigo-text">
              TARGET REALISASI PAJAK TAHUN 2021
            </h4>
            <MDBTable responsive bordered>
              <MDBTableHead>
                <tr className="text-center">
                  <th>#</th>
                  <th className="w-50">Uraian</th>
                  <th className="w-20">Target(Rp.)</th>
                  <th className="w-20">Realisasi(Rp.)</th>
                  <th>%</th>
                </tr>
              </MDBTableHead>
              <MDBTableBody>
                <tr>
                  <td>1</td>
                  <td>Cell</td>
                  <td>Cell</td>
                  <td>Cell</td>
                  <td className="bg-warning">Cell</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Cell</td>
                  <td>Cell</td>
                  <td>Cell</td>
                  <td className="bg-warning">Cell</td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Cell</td>
                  <td>Cell</td>
                  <td>Cell</td>
                  <td className="bg-warning">Cell</td>
                </tr>
              </MDBTableBody>
            </MDBTable>
          </MDBContainer>
          <MDBContainer fluid>
            <h4 className="mt-5 indigo-text">
              TARGET REALISASI RETRIBUSI TAHUN 2021
            </h4>
            <MDBTable responsive bordered>
              <MDBTableHead className="view view-cascade gradient-card-header blue-gradient d-flex justify-content-between align-items-center py-2 mx-4 mb-3">
                <tr className="text-center">
                  <th>#</th>
                  <th className="w-50">URAIAN</th>
                  <th className="w-20">TARGET(Rp.)</th>
                  <th className="w-20">REALISASI(Rp.)</th>
                  <th>%</th>
                </tr>
              </MDBTableHead>
              <MDBTableBody>
                <tr>
                  <td>1</td>
                  <td>Pajak Hotel</td>
                  <td>Cell</td>
                  <td>Cell</td>
                  <td className="bg-warning">Cell</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Cell</td>
                  <td>Cell</td>
                  <td>Cell</td>
                  <td className="bg-warning">Cell</td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Cell</td>
                  <td>Cell</td>
                  <td>Cell</td>
                  <td className="bg-warning">Cell</td>
                </tr>
              </MDBTableBody>
            </MDBTable>
          </MDBContainer>
        </div>
      </>
    );
  }
}

export default HomePage;
