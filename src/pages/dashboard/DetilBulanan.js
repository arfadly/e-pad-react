import React, { Component } from "react";
import Api from "../../services/Api";
import BulananDetail from "../../components/Realisasi/BulananDetail";
import SideNavPage from "../../components/SideNavPage";
import {
  MDBIcon,
  MDBSpinner,
  MDBBreadcrumb,
  MDBBreadcrumbItem,
  MDBContainer,
} from "mdbreact";
import Notif from "../../components/Notif";
import Bulan from "../../components/Bulan";
import { Link } from "react-router-dom";
import TopNavbar from "../../components/topNavbar";
export default class DetilBulanan extends Component {
  constructor(props) {
    super(props);
    const { state } = this.props.location;
    if (state) {
      localStorage.setItem("r2", JSON.stringify(state));
    }
    this.state = {
      data: [],
      loading: true,
      d: JSON.parse(localStorage.getItem("r2")),
    };
  }
  componentDidMount = async () => {
    console.log(this.state.d);
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    };
    await Api.get(
      "public/index.php/api/realisasi_detail_bulanan/" +
        this.state.d.data_pad.id +
        "/" +
        this.state.d.data.bulan,
      config
    )
      .then((d) => {
        if (d) {
          this.setState({
            data: d.data.rows.bulanan,
            loading: false,
          });
        }
      })
      .catch((error) => {
        Notif("e", "😇", "Belum ada data");
        this.setState({
          loading: false,
        });
        // localStorage.removeItem("token");
        // this.setState({
        //   redirect: true,
        // });
      });
  };
  render() {
    let CardDetailBulanan = "";
    if (!this.state.loading) {
      CardDetailBulanan = this.state.data.map((data, key) => {
        return (
          <BulananDetail
            data={data}
            key={key}
            refresh={this.componentDidMount}
          />
        );
      });
    }
    return (
      <>
        <TopNavbar />
        <div className="mb-32" />
        <div className="lg:px-10 xs:p-2">
          <MDBBreadcrumb
            light
            className="text-blue-700 flex justify-start align-items-center"
          >
            <div>
              <SideNavPage no={4} menu={"Realisasi PAD"} />
            </div>
            <Link
              to={{
                pathname: "/realisasi",
              }}
            >
              <MDBBreadcrumbItem icon="angle-right" className="mr-3">
                Realisasi
              </MDBBreadcrumbItem>
            </Link>
            <Link
              to={{
                pathname: "/realisasi/bulanan",
              }}
            >
              <MDBBreadcrumbItem icon="angle-right" className="mr-3">
                Bulanan
              </MDBBreadcrumbItem>
            </Link>
            <MDBBreadcrumbItem icon="angle-right" className="font-extrabold">
              Detail Bulanan
            </MDBBreadcrumbItem>
          </MDBBreadcrumb>
          <div className="mt-1"></div>

          <div className="uppercase font-bold text-blue-700 lg:text-lg xs:text-md">
            {localStorage.getItem("nama_dinas")} kota kotamobagu
          </div>

          <hr className="bg-blue-700 my-2" />
          <div className="p-5 w-full bg-gray-100 shadow-md rounded-lg overflow-hidden">
            <div className="uppercase font-bold text-blue-700 lg:text-md xs:text-md text-center">
              DAFTAR REALISASI BULANAN {" Tahun "}
              {this.state.d.data_pad.tahun}
              {" Bulan "}
              {Bulan(this.state.d.data.bulan)}
            </div>
            <div className="uppercase font-bold text-blue-700 lg:text-md xs:text-md text-center">
              {this.state.d.data_pad.nama_pad} (
              {this.state.d.data_pad.jenis_pad == 1
                ? "Retribusi"
                : this.state.d.data_pad.jenis_pad == 2
                ? "Pajak"
                : "Lainnya"}
              )
            </div>
          </div>
          <div className="bg-gray-100 rounded flex justify-left flex-wrap w-full relative">
            {!this.state.loading ? (
              CardDetailBulanan
            ) : (
              <>
                <div className="lg:w-1/4 xs:w-full">
                  <div className="m-4">
                    <div className="col-span-12 sm:col-span-6 md:col-span-3">
                      <div className="flex flex-row bg-white shadow-sm rounded p-4">
                        <MDBSpinner multicolor crazy />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="lg:w-1/4 xs:w-full">
                  <div className="m-4">
                    <div className="col-span-12 sm:col-span-6 md:col-span-3">
                      <div className="flex flex-row bg-white shadow-sm rounded p-4">
                        <MDBSpinner multicolor crazy />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="lg:w-1/4 xs:w-full">
                  <div className="m-4">
                    <div className="col-span-12 sm:col-span-6 md:col-span-3">
                      <div className="flex flex-row bg-white shadow-sm rounded p-4">
                        <MDBSpinner multicolor crazy />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="lg:w-1/4 xs:w-full">
                  <div className="m-4">
                    <div className="col-span-12 sm:col-span-6 md:col-span-3">
                      <div className="flex flex-row bg-white shadow-sm rounded p-4">
                        <MDBSpinner multicolor crazy />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="lg:w-1/4 xs:w-full">
                  <div className="m-4">
                    <div className="col-span-12 sm:col-span-6 md:col-span-3">
                      <div className="flex flex-row bg-white shadow-sm rounded p-4">
                        <MDBSpinner multicolor crazy />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="lg:w-1/4 xs:w-full">
                  <div className="m-4">
                    <div className="col-span-12 sm:col-span-6 md:col-span-3">
                      <div className="flex flex-row bg-white shadow-sm rounded p-4">
                        <MDBSpinner multicolor crazy />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="lg:w-1/4 xs:w-full">
                  <div className="m-4">
                    <div className="col-span-12 sm:col-span-6 md:col-span-3">
                      <div className="flex flex-row bg-white shadow-sm rounded p-4">
                        <MDBSpinner multicolor crazy />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="lg:w-1/4 xs:w-full">
                  <div className="m-4">
                    <div className="col-span-12 sm:col-span-6 md:col-span-3">
                      <div className="flex flex-row bg-white shadow-sm rounded p-4">
                        <MDBSpinner multicolor crazy />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="lg:w-1/4 xs:w-full">
                  <div className="m-4">
                    <div className="col-span-12 sm:col-span-6 md:col-span-3">
                      <div className="flex flex-row bg-white shadow-sm rounded p-4">
                        <MDBSpinner multicolor crazy />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="lg:w-1/4 xs:w-full">
                  <div className="m-4">
                    <div className="col-span-12 sm:col-span-6 md:col-span-3">
                      <div className="flex flex-row bg-white shadow-sm rounded p-4">
                        <MDBSpinner multicolor crazy />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="lg:w-1/4 xs:w-full">
                  <div className="m-4">
                    <div className="col-span-12 sm:col-span-6 md:col-span-3">
                      <div className="flex flex-row bg-white shadow-sm rounded p-4">
                        <MDBSpinner multicolor crazy />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="lg:w-1/4 xs:w-full">
                  <div className="m-4">
                    <div className="col-span-12 sm:col-span-6 md:col-span-3">
                      <div className="flex flex-row bg-white shadow-sm rounded p-4">
                        <MDBSpinner multicolor crazy />
                      </div>
                    </div>
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
      </>
    );
  }
}
