import React, { Component } from "react";
import {
  MDBTypography,
  MDBContainer,
  MDBBox,
  MDBSpinner,
  MDBBreadcrumb,
  MDBBreadcrumbItem,
} from "mdbreact";
import SideNavPage from "../../components/SideNavPage";
import Api from "../../services/Api";
import Notif from "../../components/Notif";
import Card from "../../components/dashboard/Card";
import pencapaianPNG from "../../assets/sandi/pencapaian.png";
import targetPNG from "../../assets/sandi/target.png";
import sisaPNG from "../../assets/sandi/sisa.png";
import TopNavbar from "../../components/topNavbar";

export default class dashboard extends Component {
  state = {
    data: [],
    skpdData: [],
    loading: true,
  };
  componentDidMount = async () => {
    setTimeout(async () => {
      const config = {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token"),
        },
      };
      await Api.get("public/index.php/api/realisasi_total", config)
        .then((d) =>
          this.setState({
            data: d.data,
            loading: false,
          })
        )
        .catch((error) => {
          // Notif("d", "😇", "Anda telah keluar aplikasi");
          //      localStorage.removeItem("token");
          this.setState({
            redirect: true,
          });
        });
      await Api.get("public/index.php/api/realisasi_skpd", config)
        .then((d) =>
          this.setState({
            skpdData: d.data.rows,
            loading: false,
          })
        )
        .catch((error) => {
          //Notif("d", "😇", "Anda telah keluar aplikasi");
          //      localStorage.removeItem("token");
          this.setState({
            redirect: true,
          });
        });
    }, 1000);
  };
  numberFormat = (value) =>
    new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
    }).format(value);
  render() {
    let skpdCard = "";
    if (!this.state.loading) {
      skpdCard = this.state.skpdData.map((data, key) => {
        return Card(data, key);
      });
    }

    return (
      <>
        <TopNavbar />
        <div className="mb-20" />
        <div className="xs:p-4 lg:p-9">
          <MDBBreadcrumb
            light
            className="text-blue-700 flex justify-start align-items-center"
          >
            <div>
              <SideNavPage no={1} menu={"Dashboard"} />
            </div>
            <MDBBreadcrumbItem icon="angle-right" className="font-extrabold">
              Dashboard
            </MDBBreadcrumbItem>
          </MDBBreadcrumb>
          <div>
            <div className="text-blue-700">
              <div className="font-bold">
                DAFTAR REALISASI PAJAK DAN RETRIBUSI
              </div>
              <div className="font-bold">PEMERINTAH KOTA KOTAMOBAGU</div>
            </div>
            <hr className="bg-blue-700" />
            <br />
            <div className="flex flex-wrap">
              <div className="z-0 lg:w-1/3 xs:w-full bg-fixed relative">
                <img src={targetPNG} className="" />
                <div className="absolute lg:top-12 lg:left-32 xs:top-9 xs:left-24 text-white font-bold text-xl">
                  TARGET
                </div>
                <div className="absolute lg:top-32 xs:top-24 xs:left-9 lg:left-12 text-white font-bold">
                  {this.state.loading ? (
                    <MDBSpinner multicolor crazy />
                  ) : (
                    <p className="font-extrabold lg:text-xl xs:text-md ">
                      {this.numberFormat(this.state.data.target)}
                    </p>
                  )}
                </div>
              </div>
              <div className="z-0 lg:w-1/3 xs:w-full bg-fixed relative">
                <img src={pencapaianPNG} className="" />
                <div className="absolute lg:top-12 lg:left-32 xs:top-9 xs:left-24 text-white font-bold text-xl">
                  REALISASI
                </div>
                <div className="absolute lg:top-32 xs:top-24 xs:left-9 lg:left-12 text-white font-bold">
                  {this.state.loading ? (
                    <MDBSpinner multicolor crazy />
                  ) : (
                    <p className="font-extrabold lg:text-2xl xs:text-lg">
                      {this.numberFormat(this.state.data.realisasi)}
                    </p>
                  )}
                </div>
                <div className="absolute lg:top-32 lg:right-16 xs:top-28 xs:right-9 text-white font-extrabold text-3xl">
                  {this.state.loading
                    ? ""
                    : this.state.data.capaian_persen + "%"}
                </div>
              </div>

              <div className="z-0 lg:w-1/3 xs:w-full bg-fixed relative">
                <img src={sisaPNG} className="" />
                <div className="absolute lg:top-12 lg:left-32 xs:top-9 xs:left-24 text-white font-bold text-xl">
                  BELUM REALISASI
                </div>
                <div className="absolute lg:top-32 xs:top-24 xs:left-9 lg:left-12 text-white font-bold">
                  {this.state.loading ? (
                    <MDBSpinner multicolor crazy />
                  ) : (
                    <p className="font-extrabold lg:text-xl xs:text-lg">
                      {this.numberFormat(this.state.data.belum_realisasi)}
                    </p>
                  )}
                </div>
                <div className="absolute lg:top-32 lg:right-16 xs:top-28 xs:right-9 text-white font-extrabold text-3xl">
                  {this.state.loading
                    ? ""
                    : (100 - this.state.data.capaian_persen).toFixed(2) + "%"}
                </div>
              </div>
            </div>
            {/* <div className="flex flex-col lg:flex-row relative">
            <div
              className="bg-local text-white p-8 m-1 lg:w-1/2 xs:text-center lg:text-left z-10"
              style={{
                backgroundImage: pencapaianPNG,
              }}
            >
              <p className="font-bold">TARGET</p>
              {this.state.loading ? (
                <MDBSpinner multicolor crazy
              ) : (
                <p className="font-bold text-2xl xs:text-2xl">
                  {this.numberFormat(this.state.data.target)}
                </p>
              )}
            </div>
            <div></div>
            <div className="bg-green-600 text-white p-8 m-1 lg:w-1/2 xs:text-center lg:text-left">
              <p className="font-bold">REALISASI</p>
              {this.state.loading ? (
                <MDBSpinner multicolor crazy
              ) : (
                <p className="font-bold text-2xl xs:text-2xl">
                  {this.numberFormat(this.state.data.realisasi)} (
                  {this.state.data.capaian_persen}%)
                </p>
              )}
            </div>
            <div className="bg-red-600 text-white p-8 m-1 lg:w-1/2 xs:text-center lg:text-left">
              <p className="font-bold">BELUM REALISASI</p>
              {this.state.loading ? (
                <MDBSpinner multicolor crazy
              ) : (
                <p className="font-bold text-2xl xs:text-2xl">
                  {this.numberFormat(this.state.data.belum_realisasi)} (
                  {(100 - this.state.data.capaian_persen).toFixed(2)}%)
                </p>
              )}
            </div>
          </div> */}

            <div className="text-blue-700">
              <div className="mt-5 font-bold">
                DAFTAR REALISASI RETRIBUSI/PAD OPD
              </div>
            </div>
            <hr className="bg-blue-700 mb-3" />
            <div>
              <div className="flex items-center">
                <div className="w-full">
                  <div className="flex flex-col flex-wrap lg:flex-row w-full">
                    {/* CARD */}
                    {this.state.loading ? (
                      <MDBSpinner multicolor crazy />
                    ) : (
                      skpdCard
                    )}
                    {/* END CARD */}
                  </div>
                </div>
              </div>
            </div>
            {/* <div className="mt-3 overflow-x-scroll ">
          <table className="text-center w-full">
            <thead className="bg-blue-300 rounded flex text-white w-full border-blue-500 border">
              <tr className="flex w-full">
                <th className="p-3 w-10 border-blue-500 border">#</th>
                <th className="p-3 w-96 border-blue-500 border">
                  Dinas Penanggung Jawab
                </th>
                <th className="p-3 w-96 border-blue-500 border">Target</th>
                <th className="p-3 w-96 border-blue-500 border">Realisasi</th>
                <th className="p-1 w-72 border-blue-500 border">Capaian</th>
              </tr>
            </thead>

            <tbody className="bg-grey-light text-left flex flex-col items-center justify-between  w-full border-blue-500 border">
              <tr className="flex w-full bg-green-200 align-item-center">
                <td className="p-3 w-10 border-blue-500 border">1</td>
                <td className="p-3 w-96 border-blue-500 border whitespace-nowrap">
                  Dinas Komunikasi dan Informatika
                </td>
                <td className="p-3 w-96 border-blue-500 border ">
                  1.200.000.000
                </td>
                <td className="p-3 w-96 border-blue-500 border">
                  1.200.000.000
                </td>
                <td className="p-1 w-72 border-blue-500 border text-2xl">
                  100%
                </td>
              </tr>
            </tbody>
          </table>
        </div> */}
          </div>

          {/* <MDBContainer className="p-3">
        <MDBTypography tag="h1" className="h4-responsive text-primary">
          DAFTAR REALIASSI PAJAK/RESTRIBUSI
        </MDBTypography>
        <MDBTypography tag="h1" className="h2-responsive text-primary">
          KOTA KOTAMOBAGU
        </MDBTypography>
        <hr className="p-2" />
        <MDBBox display="flex" justifyContent="center">
          <MDBBox bgColor="primary" className="p-5 text-center">
            <MDBTypography className="h6-responsive text-white">
              TARGET PAD 2021
            </MDBTypography>
            <MDBTypography className="h1-responsive text-white">
              Rp 1.200.000
            </MDBTypography>
          </MDBBox>
          <MDBBox bgColor="default" className="p-5 text-center">
            <MDBTypography className="h6-responsive text-white">
              REALISASI PAD 2021
            </MDBTypography>
            <MDBTypography className="h1-responsive text-white">
              Rp 1.200.000
            </MDBTypography>
          </MDBBox>
        </MDBBox>
      </MDBContainer> */}
        </div>
      </>
    );
  }
}
