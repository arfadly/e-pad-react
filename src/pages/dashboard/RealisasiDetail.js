import React, { Component } from "react";
import SideNavPage from "../../components/SideNavPage";
import Api from "../../services/Api";
import Notif from "../../components/Notif";
import {
  MDBIcon,
  MDBSpinner,
  MDBBreadcrumb,
  MDBBreadcrumbItem,
} from "mdbreact";
import { Redirect, Link } from "react-router-dom";
import StoreModal from "../../components/Realisasi/StoreModal";
import TopNavbar from "../../components/topNavbar";
import Bulanan from "../../components/Realisasi/Bulanan";

export default class RealisasiDetail extends Component {
  constructor(props) {
    super(props);
    const { state } = this.props.location;
    if (state) {
      localStorage.setItem("r1", JSON.stringify(state));
    }
    this.state = {
      data: [],
      loading: true,
      d: JSON.parse(localStorage.getItem("r1")),
    };
  }
  componentDidMount = async () => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    };
    await Api.get(
      "public/index.php/api/realisasi_bulanan/" + this.state.d.id,
      config
    )
      .then((d) => {
        if (d) {
          this.setState({
            data: d.data.rows.bulanan,
            loading: false,
          });
        }
      })
      .catch((error) => {
        Notif("e", "😇", "Belum ada data");
        this.setState({
          loading: false,
        });
        // localStorage.removeItem("token");
        // this.setState({
        //   redirect: true,
        // });
      });
  };
  render() {
    let CardBulan = "";
    if (!this.state.loading && this.state.data) {
      CardBulan = this.state.data.map((data, key) => {
        let Warna = "red";
        if (data.capaian_persen > 30 && data.capaian_persen < 70) {
          Warna = "yellow";
        }
        if (data.capaian_persen > 70) {
          Warna = "green";
        }
        return (
          <Bulanan
            data={data}
            data_pad={this.state.d}
            key={key}
            warna={Warna}
          />
        );
      });
    }
    return (
      <>
        <TopNavbar />
        <div className="mb-32" />
        <div className="lg:px-10 xs:p-2">
          <MDBBreadcrumb
            light
            className="text-blue-700 flex justify-start align-items-center"
          >
            <div>
              <SideNavPage no={4} menu={"Realisasi"} />
            </div>
            <Link
              to={{
                pathname: "/realisasi",
              }}
            >
              <MDBBreadcrumbItem icon="angle-right" className="mr-3">
                Realisasi
              </MDBBreadcrumbItem>
            </Link>
            <MDBBreadcrumbItem icon="angle-right" className="font-extrabold">
              Bulanan
            </MDBBreadcrumbItem>
          </MDBBreadcrumb>
          <div className="uppercase font-bold text-blue-700 lg:text-md xs:text-sm">
            DAFTAR REALISASI BULANAN Tahun{" "}
            {!this.state.loading ? this.state.d.tahun : ""}
          </div>
          <div className="uppercase font-bold text-blue-700 lg:text-lg xs:text-md">
            {localStorage.getItem("nama_dinas")} kota kotamobagu
          </div>

          <hr className="bg-blue-700 my-2" />
          <div className="relative">
            <form className="">
              <div className="flex flex-row lg:w-full xs:w-full">
                <div className="lg:w-1/2 xs:w-60">
                  <input
                    type="text"
                    name="cari"
                    className="focus:bg-green-100 flex-1 block ring-2 p-2 focus:border-blue-700 border m-2 w-full"
                    placeholder="Cari Target PAD"
                    onChange={this.ganti}
                  />
                </div>

                <div className="lg:w-20 ml-2 xs:w-10">
                  <button className="bg-blue-600 hover:bg-blue-300 text-white flex-1 focus:ring-2 p-2 focus:border-blue-700 border m-2 w-full">
                    <MDBIcon icon="search" />
                  </button>
                </div>
                <div className="absolute right-0">
                  <button
                    type="button"
                    className="bg-green-600 hover:bg-green-300 text-white flex-1 focus:ring-2 p-2  m-2 lg:w-20 ml-2 xs:w-10 top-0"
                  >
                    <MDBIcon icon="plus" />
                  </button>
                </div>
                <StoreModal
                  refresh={this.componentDidMount}
                  pad_id={this.state.d.id}
                />
              </div>
            </form>
          </div>
          <div className="p-5 w-full bg-gray-100 shadow-md rounded-lg overflow-hidden">
            <div className="uppercase font-bold text-blue-700 lg:text-lg xs:text-md text-center">
              {this.state.d.nama_pad} (
              {this.state.d.jenis_pad == 1
                ? "Retribusi"
                : this.state.d.jenis_pad == 2
                ? "Pajak"
                : "Lainnya"}
              )
            </div>
          </div>
          <div className="bg-gray-100 rounded flex justify-left flex-wrap w-full relative">
            {!this.state.loading ? (
              CardBulan
            ) : (
              <>
                <div className="lg:w-1/4 xs:w-full">
                  <div className="m-4">
                    <div className="col-span-12 sm:col-span-6 md:col-span-3">
                      <div className="flex flex-row bg-white shadow-sm rounded p-4">
                        <MDBSpinner multicolor crazy />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="lg:w-1/4 xs:w-full">
                  <div className="m-4">
                    <div className="col-span-12 sm:col-span-6 md:col-span-3">
                      <div className="flex flex-row bg-white shadow-sm rounded p-4">
                        <MDBSpinner multicolor crazy />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="lg:w-1/4 xs:w-full">
                  <div className="m-4">
                    <div className="col-span-12 sm:col-span-6 md:col-span-3">
                      <div className="flex flex-row bg-white shadow-sm rounded p-4">
                        <MDBSpinner multicolor crazy />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="lg:w-1/4 xs:w-full">
                  <div className="m-4">
                    <div className="col-span-12 sm:col-span-6 md:col-span-3">
                      <div className="flex flex-row bg-white shadow-sm rounded p-4">
                        <MDBSpinner multicolor crazy />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="lg:w-1/4 xs:w-full">
                  <div className="m-4">
                    <div className="col-span-12 sm:col-span-6 md:col-span-3">
                      <div className="flex flex-row bg-white shadow-sm rounded p-4">
                        <MDBSpinner multicolor crazy />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="lg:w-1/4 xs:w-full">
                  <div className="m-4">
                    <div className="col-span-12 sm:col-span-6 md:col-span-3">
                      <div className="flex flex-row bg-white shadow-sm rounded p-4">
                        <MDBSpinner multicolor crazy />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="lg:w-1/4 xs:w-full">
                  <div className="m-4">
                    <div className="col-span-12 sm:col-span-6 md:col-span-3">
                      <div className="flex flex-row bg-white shadow-sm rounded p-4">
                        <MDBSpinner multicolor crazy />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="lg:w-1/4 xs:w-full">
                  <div className="m-4">
                    <div className="col-span-12 sm:col-span-6 md:col-span-3">
                      <div className="flex flex-row bg-white shadow-sm rounded p-4">
                        <MDBSpinner multicolor crazy />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="lg:w-1/4 xs:w-full">
                  <div className="m-4">
                    <div className="col-span-12 sm:col-span-6 md:col-span-3">
                      <div className="flex flex-row bg-white shadow-sm rounded p-4">
                        <MDBSpinner multicolor crazy />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="lg:w-1/4 xs:w-full">
                  <div className="m-4">
                    <div className="col-span-12 sm:col-span-6 md:col-span-3">
                      <div className="flex flex-row bg-white shadow-sm rounded p-4">
                        <MDBSpinner multicolor crazy />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="lg:w-1/4 xs:w-full">
                  <div className="m-4">
                    <div className="col-span-12 sm:col-span-6 md:col-span-3">
                      <div className="flex flex-row bg-white shadow-sm rounded p-4">
                        <MDBSpinner multicolor crazy />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="lg:w-1/4 xs:w-full">
                  <div className="m-4">
                    <div className="col-span-12 sm:col-span-6 md:col-span-3">
                      <div className="flex flex-row bg-white shadow-sm rounded p-4">
                        <MDBSpinner multicolor crazy />
                      </div>
                    </div>
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
      </>
    );
  }
}
