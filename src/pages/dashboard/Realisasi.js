import React, { Component } from "react";
import SideNavPage from "../../components/SideNavPage";

import Cards from "../../components/Realisasi/Card";
import Api from "../../services/Api";
import {
  MDBIcon,
  MDBSpinner,
  MDBBreadcrumb,
  MDBBreadcrumbItem,
} from "mdbreact";
import Notif from "../../components/Notif";
import { Redirect } from "react-router-dom";
import TopNavbar from "../../components/topNavbar";

export default class Realisasi extends Component {
  state = {
    data: [],
    loading: true,
    skpdId: this.props.location.state ? this.props.location.state.skpd_id : 1,
  };
  componentDidMount = async () => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    };
    let host = "public/index.php/api/realisasi";
    if (localStorage.getItem("role") === "1") {
      host = "public/index.php/api/realisasi?skpd_id=" + this.state.skpdId;
    }
    await Api.get(host, config)
      .then((d) => {
        if (d) {
          this.setState({
            data: d.data.data,
            loading: false,
          });
        }
      })
      .catch((error) => {
        Notif("e", "😇", "Ada Kesalahan");
        // localStorage.removeItem("token");
        // this.setState({
        //   redirect: true,
        // });
      });
  };
  numberFormat = (value) =>
    new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
    }).format(value);
  render() {
    if (this.state.redirect) {
      return <Redirect to="/" />;
    }
    let Loading = "";
    if (this.state.loading) {
      Loading = (
        <div className="flex justify-center w-full">
          <MDBSpinner crazy />
        </div>
      );
    }
    let dataTable = (
      // <div className="flex justify-center w-full">🔎 Mencari Data...</div>
      <div className="flex justify-center w-full">
        <MDBSpinner crazy />
      </div>
    );
    if (!this.state.loading) {
      dataTable = this.state.data.map((data, key) => {
        return Cards(data, key);
      });
    }
    return (
      <>
        <TopNavbar />
        <div className="mb-32" />
        <div className="lg:px-10 xs:p-2">
          <MDBBreadcrumb
            light
            className="text-blue-700 flex justify-start align-items-center"
          >
            <div>
              <SideNavPage no={4} menu={"Realisasi"} />
            </div>
            <MDBBreadcrumbItem icon="angle-right" className="font-extrabold">
              Realisasi
            </MDBBreadcrumbItem>
          </MDBBreadcrumb>
          <div>
            <div className="uppercase font-bold text-blue-700 lg:text-md xs:text-sm">
              Target Pajak/Retribusi
            </div>
            <div className="uppercase font-bold text-blue-700 lg:text-lg xs:text-md">
              {localStorage.getItem("nama_dinas")} Pemerintah Kotamobagu
            </div>
            <hr className="bg-blue-700 my-2" />

            <div className="pt-5">
              <div className="flex justify-center lg:flex-row xs:flex-col p-5 flex-wrap bg-gray-100 shadow-md rounded-lg overflow-hidden">
                {dataTable}
              </div>
              {/* {Loading} */}

              {/* <div className="my-2 py-0 overflow-x-auto xs:mx-1 xs:px-1 lg:mx-2 pr-10 lg:px-2">
                <div className="align-middle inline-block min-w-full shadow overflow-hidden bg-white shadow-dashboard px-3 pt-3 rounded-bl-lg rounded-br-lg">
                  <table className="min-w-full">
                    <thead>
                      <tr>
                        <th className="px-1 py-3 border-b-2 border-gray-300 text-left leading-4 text-blue-500 tracking-wider uppercase">
                          #
                        </th>
                        <th className="px-1 py-3 border-b-2 border-gray-300 text-left text-sm leading-4 text-blue-500 tracking-wider uppercase">
                          Nama PAD
                        </th>
                        <th className="px-1 py-3 border-b-2 border-gray-300 text-left text-sm leading-4 text-blue-500 tracking-wider uppercase">
                          Jenis PAD
                        </th>
                        <th className="px-1 py-3 border-b-2 border-gray-300 text-left text-sm leading-4 text-blue-500 tracking-wider uppercase">
                          Target
                        </th>
                        <th className="px-1 py-3 border-b-2 border-gray-300 text-left text-sm leading-4 text-blue-500 tracking-wider uppercase">
                          Tahun
                        </th>
                        <th className="px-1 py-3 border-b-2 border-gray-300 text-left text-sm leading-4 text-blue-500 tracking-wider"></th>
                      </tr>
                    </thead>
                    <tbody className="bg-white">
                      {dataTable}
                    </tbody>
                  </table>
                  <div className="flex justify-center p-3">{Loading}</div>
                  <div className="w-full text-blue-700 my-1 flex justify-center">
                    <button className="hover:bg-blue-100 rounded w-1/6 p-0">
                      <MDBIcon icon="caret-down fa-2x" />
                    </button>
                  </div>
                </div>
              </div> */}
            </div>
          </div>
        </div>
      </>
    );
  }
}
