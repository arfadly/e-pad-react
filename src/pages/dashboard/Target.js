import React, { Component } from "react";
import SideNavPage from "../../components/SideNavPage";
import StoreModal from "../../components/Target/StoreModal";
import EditMOdal from "../../components/Target/EditModal";
import DeleteModal from "../../components/Target/DeleteModal";
import Api from "../../services/Api";
import {
  MDBIcon,
  MDBSpinner,
  MDBBreadcrumb,
  MDBBreadcrumbItem,
} from "mdbreact";
import Notif from "../../components/Notif";
import { Redirect } from "react-router-dom";
import TopNavbar from "../../components/topNavbar";
export default class Target extends Component {
  state = {
    data: [],
    loading: true,
  };
  componentDidMount = async () => {
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    };
    await Api.get("public/index.php/api/target", config)
      .then((d) => {
        if (d) {
          this.setState({
            data: d.data.rows,
            loading: false,
          });
        }
      })
      .catch((error) => {
        Notif("d", "😇", "Anda telah keluar aplikasi");
        // localStorage.removeItem("token");
        // this.setState({
        //   redirect: true,
        // });
      });
  };
  numberFormat = (value) =>
    new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
    }).format(value);
  render() {
    if (this.state.redirect) {
      return <Redirect to="/" />;
    }
    let Loading = "";
    if (this.state.loading) {
      Loading = <MDBSpinner multicolor crazy />;
    }
    let dataTable = (
      <tr>
        <td>❌</td>
        <td>❌</td>
        <td>❌</td>
        <td>❌</td>
      </tr>
    );
    if (!this.state.loading) {
      dataTable = this.state.data.map((data, key) => {
        return (
          <tr key={key}>
            <td className="p-2 whitespace-no-wrap border-b border-gray-500">
              <div className="flex items-center">
                <div>
                  <div className="text-sm font-semibold text-gray-800">
                    {key + 1}
                  </div>
                </div>
              </div>
            </td>
            <td className="p-2 whitespace-no-wrap border-b border-gray-500">
              <div className="text-sm font-semibold text-blue-900">
                {data.pad ? data.pad.nama_pad : null}
              </div>
            </td>
            <td className="p-2 whitespace-no-wrap border-b border-gray-500">
              <div className="text-sm font-semibold text-blue-900">
                {data.pad
                  ? data.pad.jenis_pad == 1
                    ? "Retribusi"
                    : data.pad.jenis_pad == 2
                    ? "Pajak"
                    : "Lainnya"
                  : "null"}
              </div>
            </td>
            <td className="p-2 whitespace-no-wrap border-b border-gray-500">
              <div className="text-sm font-semibold text-blue-900">
                {this.numberFormat(data.target)}
              </div>
            </td>
            <td className="p-2 whitespace-no-wrap border-b border-gray-500">
              <div className="text-sm font-semibold text-blue-900">
                {data.tahun}
              </div>
            </td>

            <td className="p-2 whitespace-no-wrap border-b border-gray-500">
              <div className="flex">
                {data.pad ? (
                  <EditMOdal refresh={this.componentDidMount} data={data} />
                ) : (
                  ""
                )}
                <DeleteModal refresh={this.componentDidMount} data={data} />
              </div>
            </td>
          </tr>
        );
      });
    }
    return (
      <>
        <TopNavbar />
        <div className="mb-32" />
        <div className="lg:px-10 xs:p-2">
          <MDBBreadcrumb
            light
            className="text-blue-700 flex justify-start align-items-center"
          >
            <div>
              <SideNavPage no={3} menu={"Target PAD"} />
            </div>
            <MDBBreadcrumbItem icon="angle-right" className="font-extrabold">
              Target PAD
            </MDBBreadcrumbItem>
          </MDBBreadcrumb>
          <div>
            <div className="uppercase font-bold text-blue-700 lg:text-md xs:text-sm">
              Target Pajak/Retribusi
            </div>
            <div className="uppercase font-bold text-blue-700 lg:text-lg xs:text-md">
              {localStorage.getItem("nama_dinas")} kota kotamobagu
            </div>
            <hr className="bg-blue-700 my-2" />
            <div className="relative">
              <form className="">
                <div className="flex flex-row lg:w-full xs:w-full">
                  <div className="lg:w-1/2 xs:w-60">
                    <input
                      type="text"
                      name="cari"
                      className="focus:bg-green-100 flex-1 block ring-2 p-2 focus:border-blue-700 border m-2 w-full"
                      placeholder="Cari Target PAD"
                      onChange={this.ganti}
                    />
                  </div>

                  <div className="lg:w-20 ml-2 xs:w-10">
                    <button className="bg-blue-600 hover:bg-blue-300 text-white flex-1 focus:ring-2 p-2 focus:border-blue-700 border m-2 w-full">
                      <MDBIcon icon="search" />
                    </button>
                  </div>
                  <div className="absolute right-0">
                    <button
                      type="button"
                      className="bg-green-600 hover:bg-green-300 text-white flex-1 focus:ring-2 p-2  m-2 lg:w-20 ml-2 xs:w-10 top-0"
                    >
                      <MDBIcon icon="plus" />
                    </button>
                  </div>
                  <StoreModal refresh={this.componentDidMount} />
                </div>
              </form>
            </div>
            <div>
              <div className="my-2 py-0 overflow-x-auto xs:mx-1 xs:px-1 lg:mx-2 pr-10 lg:px-2">
                <div className="align-middle inline-block min-w-full shadow overflow-hidden bg-white shadow-dashboard px-3 pt-3 rounded-bl-lg rounded-br-lg">
                  <table className="min-w-full font-semibold">
                    <thead>
                      <tr>
                        <th className="px-1 py-3 border-b-2 border-gray-300 text-left leading-4 text-blue-500 tracking-wider uppercase">
                          #
                        </th>
                        <th className="px-1 py-3 border-b-2 border-gray-300 text-left text-sm leading-4 text-blue-500 tracking-wider uppercase">
                          Nama PAD
                        </th>
                        <th className="px-1 py-3 border-b-2 border-gray-300 text-left text-sm leading-4 text-blue-500 tracking-wider uppercase">
                          Jenis PAD
                        </th>
                        <th className="px-1 py-3 border-b-2 border-gray-300 text-left text-sm leading-4 text-blue-500 tracking-wider uppercase">
                          Target
                        </th>
                        <th className="px-1 py-3 border-b-2 border-gray-300 text-left text-sm leading-4 text-blue-500 tracking-wider uppercase">
                          Tahun
                        </th>
                        <th className="px-1 py-3 border-b-2 border-gray-300 text-left text-sm leading-4 text-blue-500 tracking-wider"></th>
                      </tr>
                    </thead>
                    <tbody className="bg-white">
                      {dataTable}
                      {/* Tempat Table */}
                    </tbody>
                  </table>
                  <div className="flex justify-center p-3">{Loading}</div>
                  <div className="w-full text-blue-700 my-1 flex justify-center">
                    <button className="hover:bg-blue-100 rounded w-1/6 p-0">
                      <MDBIcon icon="caret-down fa-2x" />
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
