import React, { Component } from "react";
import { MDBSpinner } from "mdbreact";
import Api from "../../services/Api";
import { Redirect } from "react-router-dom";
import Notif from "../../components/Notif";
import TopNavbar from "../../components/topNavbar";
export default class login extends Component {
  state = {
    redirect: false,
    loading: false,
  };
  Login = async (e) => {
    e.preventDefault(e);
    this.setState({
      loading: true,
    });
    let fd = new FormData();
    fd.append("email", this.state.email);
    fd.append("password", this.state.password);
    await Api.post("public/index.php/api/auth", fd)
      .then((login) => {
        if (login.data.success.token) {
          //this.setState({ redirect: true });
          localStorage.setItem("token", login.data.success.token);
          localStorage.setItem("nama_dinas", login.data.success.nama_dinas);
          localStorage.setItem("role", login.data.success.role);
          this.setState({
            redirect: true,
            loading: false,
          });
          Notif("d", "✔️", "Login Berhasil");
        }
      })
      .catch((error) => {
        Notif("e", "✔️", "Login gagal");
        this.setState({
          loading: false,
        });
      });
  };
  ganti = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };
  render() {
    if (localStorage.getItem("token") || this.state.redirect) {
      return <Redirect to="/dashboard" />;
    }
    let Tombol = (
      <button
        className="ring-2 w-full m-2 rounded p-3 bg-blue-700 text-white uppercase focus:ring-4"
        type="submit"
      >
        Masuk
      </button>
    );
    if (this.state.loading) {
      Tombol = (
        <div className="flex justify-center">
          <MDBSpinner multicolor crazy />
        </div>
      );
    }
    return (
      <>
        <TopNavbar />
        <div className="p-9 h-full mt-32">
          <div className="flex justify-content-center align-items-center flex-col w-full">
            <div className="text-blue-700 font-bold mb-4 text-center h3 mt-10">
              LOGIN
            </div>
            <form className="w-full lg:w-1/3 h-full" onSubmit={this.Login}>
              <div>
                <input
                  type="text"
                  name="email"
                  className="focus:bg-green-100  flex-1 block w-full ring-2 p-3 focus:border-blue-700 border m-2"
                  placeholder="Masukkan Email"
                  onChange={this.ganti}
                />
                <input
                  type="password"
                  name="password"
                  className="focus:bg-green-100 flex-1 block w-full ring-2 p-3 focus:border-blue-700 border m-2"
                  placeholder="Mesukkan Password"
                  onChange={this.ganti}
                />
                {Tombol}
              </div>
            </form>
          </div>
        </div>
      </>
    );
  }
}
