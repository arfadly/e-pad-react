import React, { Component } from "react";
import {
  MDBBtn,
  MDBModal,
  MDBModalBody,
  MDBModalHeader,
  MDBModalFooter,
  MDBIcon,
  MDBSpinner,
} from "mdbreact";
import Notif from "../../components/Notif";
import Api from "../../services/Api";
class HapusModal extends Component {
  state = {
    modal14: false,
  };

  toggle = (nr) => () => {
    let modalNumber = "modal" + nr;
    this.setState({
      [modalNumber]: !this.state[modalNumber],
      pad_id: this.props.data.pad_id,
      loading: false,
    });
  };
  TambahTarget = async (e) => {
    e.preventDefault();
    this.setState({
      loading: true,
    });
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    };

    await Api.delete(
      "public/index.php/api/target/" + this.props.data.id,
      config
    )
      .then((d) => {
        if (d) {
          Notif("d", "✔️", "Target berhasil dihapus");
          this.props.refresh();
          this.setState({
            modal14: false,
            loading: false,
          });
        }
      })
      .catch((error) => {
        Notif("e", "😇", "oww.. ada kesalahan");
        this.setState({
          loading: false,
        });
      });
  };
  ganti = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };
  render() {
    let Tombol = (
      <div className="w-full flex justify-center mt-5 p-5">
        <button
          className="ring-2 w-full rounded p-3 bg-red-700 text-white uppercase focus:ring-4 focus:bg-yellow-500"
          onClick={this.TambahTarget}
        >
          Hapus
        </button>
      </div>
    );
    if (this.state.loading) {
      Tombol = (
        <div className="w-full flex justify-center mt-5 p-5">
          <MDBSpinner />
        </div>
      );
    }
    let PilihPAD = (
      <option className="bg-blue-300 text-lg" value="1">
        ❌
      </option>
    );
    if (this.state.pad) {
      PilihPAD = this.state.pad.map((data, key) => {
        return (
          <option className="bg-blue-300 text-lg" value={data.id} key={key}>
            {data.nama_pad} (
            {data.jenis_pad == 1
              ? "Retribusi"
              : data.jenis_pad == 2
              ? "Pajak"
              : "Lainnya"}
            )
          </option>
        );
      });
    }
    return (
      <>
        <button
          type="button"
          onClick={this.toggle(14)}
          className="px-5 py-2 m-1 border-red-500 border text-red-500 rounded transition duration-300 hover:bg-red-700 hover:text-white focus:outline-none"
        >
          <MDBIcon icon="trash" />
        </button>

        <MDBModal isOpen={this.state.modal14} toggle={this.toggle(14)} centered>
          <MDBModalHeader
            toggle={this.toggle(14)}
            className="text-lg text-blue-700 uppercase font-bold"
          >
            Hapus Target
          </MDBModalHeader>
          <div className="p-5 ">
            Apakah anda yakin akan menghapus{" "}
            <span className="font-extrabold">
              {this.props.data.pad ? this.props.data.pad.nama_pad : "????"}
            </span>{" "}
            ?
          </div>
          {Tombol}

          <div className=" text-red-400 p-1 text-center border-t">
            Note: form ini digunakan untuk menghapus target
          </div>
        </MDBModal>
      </>
    );
  }
}

export default HapusModal;
