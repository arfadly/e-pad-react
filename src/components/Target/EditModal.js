import React, { Component } from "react";
import {
  MDBBtn,
  MDBModal,
  MDBModalBody,
  MDBModalHeader,
  MDBModalFooter,
  MDBIcon,
  MDBSpinner,
} from "mdbreact";
import Notif from "../../components/Notif";
import Api from "../../services/Api";
class EditModal extends Component {
  state = {
    modal14: false,
  };

  toggle = (nr) => () => {
    let modalNumber = "modal" + nr;
    this.setState(
      {
        [modalNumber]: !this.state[modalNumber],
        pad_id: this.props.data.pad_id,
        target: this.props.data.target,
        loading: false,
      },
      async () => {
        const config = {
          headers: {
            Authorization: "Bearer " + localStorage.getItem("token"),
          },
        };
        await Api.get("public/index.php/api/pad", config)
          .then((d) => {
            if (d) {
              this.setState({
                pad: d.data.rows,
              });
            }
          })
          .catch((error) => {
            Notif("e", "😇", "Data PAD tidak ditemukan!");
          });
      }
    );
  };
  TambahTarget = async (e) => {
    e.preventDefault();
    this.setState({
      loading: true,
    });
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    };
    await Api.put(
      "public/index.php/api/target/" + this.props.data.id,
      this.state,
      config
    )
      .then((d) => {
        if (d) {
          Notif("d", "✔️", "Target berhasil dirubah");
          this.props.refresh();
          this.setState({
            modal14: false,
            loading: false,
          });
        }
      })
      .catch((error) => {
        Notif("e", "😇", "oww.. ada kesalahan");
        this.setState({
          loading: false,
        });
      });
  };
  ganti = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };
  render() {
    let Tombol = (
      <button
        className="ring-2 w-full mt-5 rounded p-3 bg-blue-700 text-white uppercase focus:ring-4 focus:bg-yellow-500"
        onClick={this.TambahTarget}
      >
        Edit Target
      </button>
    );
    if (this.state.loading) {
      Tombol = <MDBSpinner />;
    }
    let PilihPAD = (
      <option className="bg-blue-300 text-lg" value="1">
        ❌
      </option>
    );
    if (this.state.pad) {
      PilihPAD = this.state.pad.map((data, key) => {
        return (
          <option className="bg-blue-300 text-lg" value={data.id} key={key}>
            {data.nama_pad} (
            {data.jenis_pad == 1
              ? "Retribusi"
              : data.jenis_pad == 2
              ? "Pajak"
              : "Lainnya"}
            )
          </option>
        );
      });
    }
    return (
      <>
        <button
          type="button"
          onClick={this.toggle(14)}
          className="px-5 py-2 m-1 border-blue-500 border text-blue-500 rounded transition duration-300 hover:bg-blue-700 hover:text-white focus:outline-none"
        >
          <MDBIcon icon="pen" />
        </button>

        <MDBModal isOpen={this.state.modal14} toggle={this.toggle(14)} centered>
          <MDBModalHeader
            toggle={this.toggle(14)}
            className="text-lg text-blue-700 uppercase font-bold"
          >
            Edit Target
          </MDBModalHeader>

          <div className="p-5">
            <form
              className="flex flex-col justify-center w-full p-2"
              onSubmit={this.tambahPAD}
            >
              <div className="w-full  mt-3">
                <div className="text-blue-700 uppercase font-bold">
                  Pilih Jenis PAD
                </div>
                <select
                  name="pad_id"
                  className="ring border-none w-full p-3"
                  onChange={this.ganti}
                  defaultValue="1"
                >
                  <option
                    className="bg-red-300 text-lg"
                    value={this.props.data.id}
                    selected
                  >
                    {this.props.data.pad.nama_pad} (
                    {this.props.data.pad.jenis_pad == 1
                      ? "Retribusi"
                      : this.props.data.pad.jenis_pad == 2
                      ? "Pajak"
                      : "Lainnya"}
                    )
                  </option>
                  {PilihPAD}
                </select>
              </div>
              <input
                type="number"
                name="target"
                value={this.state.target}
                className="focus:bg-green-100 flex-1 block w-full ring-2 p-3 focus:border-blue-700 border mt-3"
                placeholder="Masukan Nominal Target"
                onChange={this.ganti}
                required={true}
              />

              <div className="w-full flex justify-center mt-5">{Tombol}</div>
            </form>
          </div>

          <div className=" text-red-400 p-1 text-center border-t">
            Note: form ini digunakan untuk mengubah target
          </div>
        </MDBModal>
      </>
    );
  }
}

export default EditModal;
