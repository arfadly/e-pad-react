import React from "react";

export default function Bulan(b) {
  if (b === 1) {
    return "Januari";
  }
  if (b === 2) {
    return "Februari";
  }
  if (b === 3) {
    return "Maret";
  }
  if (b === 4) {
    return "April";
  }
  if (b === 5) {
    return "Mei";
  }
  if (b === 6) {
    return "Juni";
  }
  if (b === 7) {
    return "Juli";
  }
  if (b === 8) {
    return "Agustus";
  }
  if (b === 9) {
    return "September";
  }
  if (b === 10) {
    return "Oktober";
  }
  if (b === 11) {
    return "November";
  }
  if (b === 12) {
    return "Desember";
  } else {
    return <div></div>;
  }
}
