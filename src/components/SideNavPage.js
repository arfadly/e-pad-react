import React from "react";
import Logo from "../assets/logo.png";
import {
  MDBIcon,
  MDBSideNavCat,
  MDBSideNavNav,
  MDBSideNav,
  MDBContainer,
  MDBRow,
  MDBBtn,
} from "mdbreact";
import Notif from "./Notif";
import { Link, Redirect } from "react-router-dom";

class SideNavPage extends React.Component {
  state = {
    isOpen: false,
    aktif:
      "ring-2 ring-yellow-500 text-sm w-2/3 rounded p-2 hover:bg-yellow-200 hover:text-blue-700 uppercase",
    default:
      "text-sm w-2/3 rounded p-2 hover:bg-yellow-200 hover:text-blue-700 uppercase",
  };

  handleToggle = () => {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  };
  logout = () => {
    Notif("d", "✔️", "Anda berhasil keluar");
    localStorage.removeItem("token");
    localStorage.removeItem("nama_dinas");
    localStorage.removeItem("role");
    this.setState({
      redirect: true,
    });
  };
  render() {
    const { isOpen } = this.state;
    if (this.state.redirect) {
      return <Redirect to="/" />;
    }
    if (localStorage.getItem("role") === "2") {
      return (
        <MDBContainer fluid>
          <MDBRow>
            <MDBBtn
              onClick={this.handleToggle}
              className="px-3 py-2"
              color="primary"
            >
              <MDBIcon icon="bars" />
            </MDBBtn>
          </MDBRow>
          <MDBSideNav
            hidden
            triggerOpening={isOpen}
            breakWidth={1300}
            className="indigo"
          >
            <div className="d-flex flex-col p-2 justify-content-center align-items-center">
              <div>
                <img src={Logo} style={{ width: "50px" }} />
              </div>
              <div className="d-flex flex-col">
                <div className="text-warning text-md uppercase">
                  {this.props.menu}
                </div>
              </div>
            </div>
            <MDBSideNavNav>
              <Link to="/">
                <div
                  className={
                    this.props.no === 10 ? this.state.aktif : this.state.default
                  }
                >
                  Beranda
                </div>
              </Link>
              <Link to="/dashboard">
                <div
                  className={
                    this.props.no === 1 ? this.state.aktif : this.state.default
                  }
                >
                  Dashboard
                </div>
              </Link>
              <Link to="/jenisPAD">
                <div
                  className={
                    this.props.no === 2 ? this.state.aktif : this.state.default
                  }
                >
                  Jenis PAD
                </div>
              </Link>
              <Link to="/target">
                <div
                  className={
                    this.props.no === 3 ? this.state.aktif : this.state.default
                  }
                >
                  Target PAD
                </div>
              </Link>
              <Link to="/realisasi">
                <div
                  className={
                    this.props.no === 4 ? this.state.aktif : this.state.default
                  }
                >
                  Realisasi PAD
                </div>
              </Link>

              {/* <Link to="/pengguna">
                <div
                  className={
                    this.props.no === 5 ? this.state.aktif : this.state.default
                  }
                >
                  Pengguna
                </div>
              </Link> */}
              <Link to="#">
                <div
                  className="text-sm w-2/3 rounded p-2 hover:bg-yellow-200 hover:text-blue-700 uppercase"
                  onClick={this.logout}
                >
                  Keluar
                </div>
              </Link>
            </MDBSideNavNav>
          </MDBSideNav>
        </MDBContainer>
      );
    }
    if (localStorage.getItem("role") === "1") {
      return (
        <MDBContainer fluid>
          <MDBRow>
            <MDBBtn
              onClick={this.handleToggle}
              className="px-3 py-2"
              color="primary"
            >
              <MDBIcon icon="bars" />
            </MDBBtn>
          </MDBRow>
          <MDBSideNav
            hidden
            triggerOpening={isOpen}
            breakWidth={1300}
            className="indigo"
          >
            <div className="d-flex flex-col p-4 justify-content-center align-items-center">
              <div>
                <img src={Logo} style={{ width: "50px" }} />
              </div>
              <div className="d-flex flex-col">
                <div className="text-warning text-md uppercase">
                  {this.props.menu}
                </div>
              </div>
            </div>
            <MDBSideNavNav>
              <Link to="/dashboard">
                <div
                  className={
                    this.props.no === 1 ? this.state.aktif : this.state.default
                  }
                >
                  Dashboard
                </div>
              </Link>

              <Link to="#">
                <div
                  className="text-sm w-2/3 rounded p-2 hover:bg-yellow-200 hover:text-blue-700 uppercase"
                  onClick={this.logout}
                >
                  Keluar
                </div>
              </Link>
            </MDBSideNavNav>
          </MDBSideNav>
        </MDBContainer>
      );
    } else {
      return "";
    }
  }
}

export default SideNavPage;
