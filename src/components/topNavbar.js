import React, { Component } from "react";
import {
  MDBNavbar,
  MDBNavbarBrand,
  MDBNavbarNav,
  MDBNavbarToggler,
  MDBNavItem,
} from "mdbreact";
import Logo from "../assets/logoStroke.png";
import { Link } from "react-router-dom";

export default class topNavbar extends Component {
  render() {
    let Button = (
      <Link to="/dashboard">
        <button className="ring-2 lg:p-2 xs:p-1 bg-blue-800 text-white uppercase ring-yellow-500 hover:bg-yellow-700 rounded lg:w-30 xs:w-15 lg:text-lg xs:text-xs">
          {localStorage.getItem("nama_dinas")}
        </button>
      </Link>
    );
    if (!localStorage.getItem("token")) {
      Button = (
        <Link to="/login">
          <button className="ring-2 lg:p-2 xs:p-1 text-white uppercase ring-yellow-500 hover:bg-yellow-700 rounded lg:w-20 xs:w-15 lg:text-lg xs:text-xs">
            Login
          </button>
        </Link>
      );
    }
    return (
      <>
        <MDBNavbar color="indigo" fixed="top">
          <MDBNavbarBrand>
            <div className="d-flex flex-row align-content-left  ">
              <Link to="/">
                <img src={Logo} style={{ width: "100px" }} />
                <div className="text-warning p-0 m-0 lg:text-xl xs:text-sm">
                  PEMERINTAH KOTAMOBAGU
                </div>
              </Link>
              <div className="d-flex flex-column ml-2">
                {/* <div className="text-warning p-0 m-0 lg:text-xl xs:text-sm">
                      e-PAD
                    </div> */}
                {/* <div className="text-warning p-0 m-0 lg:text-xl xs:text-sm">
                      PEMERINTAH KOTAMOBAGU
                    </div> */}
              </div>
            </div>
          </MDBNavbarBrand>
          <div className="absolute lg:right-5 lg:top-5 xs:top-20 xs:right-16 lg:contents">
            {Button}
          </div>
        </MDBNavbar>
      </>
    );
  }
}
