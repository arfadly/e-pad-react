import React from "react";
import { Link } from "react-router-dom";
export default function Card(data, key) {
  const numberFormat = (value) =>
    new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
    }).format(value);
  return (
    <Link
      className="w-full lg:w-1/4 relative"
      key={key}
      to={{
        pathname: "/realisasi",
        state: data,
      }}
    >
      <div className="m-1">
        <div className="widget p-4 rounded-lg border border-gray-300 dark:bg-gray-900 dark:border-gray-800 flex flex-row items-center justify-between w-full">
          <div className="flex flex-col w-full">
            <div className="text-sm uppercase font-bold text-blue-700 w-5/6">
              {data.name}
            </div>
            <hr />
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              className="text-blue-700 w-10 absolute right-2 top-1"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M9 19v-6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2a2 2 0 002-2zm0 0V9a2 2 0 012-2h2a2 2 0 012 2v10m-6 0a2 2 0 002 2h2a2 2 0 002-2m0 0V5a2 2 0 012-2h2a2 2 0 012 2v14a2 2 0 01-2 2h-2a2 2 0 01-2-2z"
              />
            </svg>
            <div className="flex flex-col justify-content-lg-around mt-1 ">
              <div className=" rounded p-2 text-blue-700 text-center">
                <div className="text-sm">
                  <span>TARGET </span>
                </div>
                <span className="font-bold text-lg">
                  {numberFormat(data.target)}
                </span>
              </div>
              <div className="flex flex-row justify-between">
                <div className="  rounded p-2 text-green-700 flex-col text-center">
                  <span className="text-sm">REALISASI</span>
                  <div className="text-sm font-bold  ">
                    {numberFormat(data.realisasi)}
                  </div>
                  <div className="text-md font-bold  ">
                    ({data.capaian_persen}%)
                  </div>
                </div>

                <div className="  rounded p-2 text-red-700 flex-col text-center">
                  <span className="text-sm">BELUM REALISASI</span>
                  <div className="text-sm font-bold ">
                    {numberFormat(data.belum_realisasi)}
                  </div>
                  <div className="text-md font-bold ">
                    ({(100 - data.capaian_persen).toFixed(2)}%)
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Link>
  );
}
