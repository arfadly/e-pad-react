import React from "react";
import { Link } from "react-router-dom";

export default function Card(d, key) {
  let Warna =
    d.capaian_persen < 35
      ? "red"
      : d.capaian_persen >= 30 && d.capaian_persen < 70
      ? "yellow"
      : "green";
  const numberFormat = (value) =>
    new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
    }).format(value);
  return (
    <Link
      to={{
        pathname: "/realisasi/bulanan",
        state: d,
      }}
      className="relative flex flex-col items-center justify-start lg:m-5 xs:m-0 lg:w-1/5 xs:w-full rounded-2xl p-4"
      style={{
        WebkitTransform: "translate(0px, 0px)",
        msTransform: "translate(0px, 0px)",
        transform: "translate(0px, 0px)",
        opacity: "1",
      }}
      key={key}
    >
      <div
        className={
          "absolute z-0 w-full h-full text-white transform scale-x-105 scale-y-95 bg-" +
          Warna +
          "-300 rounded-xl -rotate-2 "
        }
        style={{ zIndex: "-1" }}
      />
      <div
        className={
          "absolute z-0 w-full h-full text-white transform scale-x-105 scale-y-95 bg-" +
          Warna +
          "-400 rounded-xl rotate-2 "
        }
        style={{ zIndex: "-1" }}
      />
      <div
        className="absolute z-0 w-full h-full transform scale-x-105 scale-y-95 bg-white rounded-xl "
        style={{ zIndex: "-1" }}
      />
      <h3
        className={"z-10 p-2 text-lg font-semibold text-" + Warna + "-600 mt-5"}
      >
        {d.nama_pad}
      </h3>
      <div className="flex flex-row text-gray-600 z-10 justify-between w-full mb-2">
        <div>
          {d.jenis_pad == 1
            ? "Retribusi"
            : d.jenis_pad == 2
            ? "Pajak"
            : "Lainnya"}
        </div>
        <div>{d.tahun}</div>
      </div>
      <div
        className={
          "text-" +
          Warna +
          "-900 z-10 text-center border border-" +
          Warna +
          "-200 w-full rounded-lg p-2"
        }
      >
        <div>TARGET</div>
        <div className="text-md font-bold">{numberFormat(d.target)}</div>
      </div>
      <div
        className={
          "text-" +
          Warna +
          "-900 z-10 text-center border border-" +
          Warna +
          "-200 w-full rounded-lg p-2"
        }
      >
        <div>REALISASI</div>
        <div className="text-md font-bold">
          {numberFormat(d.realisasi)} ({d.capaian_persen}%)
        </div>
      </div>
      <div
        className={
          "text-" +
          Warna +
          "-900 z-10 text-center border border-" +
          Warna +
          "-200 w-full rounded-lg p-2"
        }
      >
        <div>BELUM REALISASI</div>
        <div className="text-md font-bold">
          {numberFormat(d.belum_realisasi)} (
          {d.capaian_persen - 100 > 100
            ? "Melebihi "
            : (d.capaian_persen - 100).toFixed(2)}
          %)
        </div>
      </div>
    </Link>
  );
}
