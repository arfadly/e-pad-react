import React, { Component } from "react";
import { MDBSpinner, MDBIcon } from "mdbreact";
import EditMOdal from "../../components/Realisasi/EditModal";
import DeleteModal from "../../components/Realisasi/DeleteModal";

export default class BulananDetail extends Component {
  render() {
    return (
      <div className="lg:w-1/4 xs:w-full" key={this.props.key}>
        <div className="m-4 relative">
          <EditMOdal data={this.props.data} refresh={this.props.refresh} />
          <DeleteModal data={this.props.data} refresh={this.props.refresh} />
          <div className="col-span-12 sm:col-span-6 md:col-span-3">
            <div className="flex flex-row bg-white shadow-sm rounded p-4">
              <div
                className={
                  "flex items-center justify-center flex-shrink-0 h-15 w-20 rounded-xl bg-blue-100 font-bold text-blue-500 p-5"
                }
              >
                {this.props.data.capaian_persen}%
              </div>
              <div className="flex flex-col flex-grow ml-10">
                <a
                  className="text-md text-gray-500 cursor-pointer font-bold"
                  href={
                    "https://pad.kotamobagukota.go.id/api/public/index.php/api/file/" +
                    this.props.data.sts_file
                  }
                  target="_blank"
                >
                  <MDBIcon icon="file-pdf" className="text-blue-700" />
                  {" " + "PDF File"}
                </a>
                <div className="text-md text-gray-500">
                  <MDBIcon far icon="calendar-alt" className="text-blue-700" />
                  {" " + this.props.data.tanggal}
                </div>
                <div className="text-md text-gray-500">
                  <MDBIcon
                    far
                    icon="calendar-check"
                    className="text-blue-700"
                  />
                  {" " + this.props.data.tanggal}
                </div>
                <div className="font-bold text-lg">
                  Rp {this.props.data.realisasi}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
