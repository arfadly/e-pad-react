import React, { Component } from "react";
import {
  MDBBtn,
  MDBModal,
  MDBModalBody,
  MDBModalHeader,
  MDBModalFooter,
  MDBIcon,
  MDBSpinner,
} from "mdbreact";
import Notif from "../../components/Notif";
import Api from "../../services/Api";
class StoreModal extends Component {
  state = {
    modal14: false,
  };

  toggle = (nr) => () => {
    const current = new Date();
    let modalNumber = "modal" + nr;
    this.setState({
      [modalNumber]: !this.state[modalNumber],
      pad_id: this.props.pad_id,
      loading: false,
      berhasil: false,
      // tanggal: `${current.getFullYear()}-${
      //   current.getMonth() + 1
      // }-${current.getDate()}`,
      // tanggal: `${current.getDate()}/${
      //   current.getMonth() + 1
      // }/${current.getFullYear()}`,
    });
  };
  Tambah = async (e) => {
    e.preventDefault();
    this.setState({
      loading: true,
    });
    if (
      !this.state.pad_id ||
      !this.state.tanggal ||
      !this.state.total ||
      !this.state.sts_file ||
      !this.state.tanggal_sts
    ) {
      Notif("e", "❌", "Form Tidak Boleh Kosong");
      this.setState({
        loading: false,
      });
    } else {
      const config = {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token"),
        },
      };
      let fd = new FormData(); //formdata object
      fd.append("file", this.state.file); //append the values with key, value pair
      await Api.post("public/index.php/api/upload", fd, config)
        .then((d) => {
          if (d) {
            Notif("d", "✔️", "File berhasil diupload");
            this.props.refresh();
            this.setState(
              {
                sts_file: d.data.file,
              },
              async () => {
                await Api.post(
                  "public/index.php/api/realisasi",
                  this.state,
                  config
                )
                  .then((d) => {
                    if (d) {
                      Notif("d", "✔️", "Realiasi berhasil ditambahkan");
                      this.props.refresh();
                      this.setState({
                        modal14: false,
                        loading: false,
                      });
                    }
                  })
                  .catch((error) => {
                    Notif("e", "😇", "oww.. Realisasi gagal ditambahkan");
                    this.setState({
                      loading: false,
                    });
                  });
              }
            );
          }
        })
        .catch((error) => {
          Notif("e", "😇", "oww.. File gagal diupload");
        });
    }
  };
  ganti = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };
  render() {
    let Tombol = (
      <button
        className="ring-2 w-full mt-5 rounded p-3 bg-blue-700 text-white uppercase focus:ring-4 focus:bg-yellow-500"
        onClick={this.Tambah}
      >
        Tambah Realisasi
      </button>
    );
    if (this.state.loading) {
      Tombol = <MDBSpinner />;
    }

    return (
      <>
        <div className="absolute right-0">
          <button
            type="button"
            className="bg-green-600 hover:bg-green-300 text-white flex-1 focus:ring-2 p-2  m-2 lg:w-20 ml-2 xs:w-10 top-0 "
            onClick={this.toggle(14)}
          >
            <MDBIcon icon="plus" />
          </button>
        </div>
        <MDBModal isOpen={this.state.modal14} toggle={this.toggle(14)} centered>
          <MDBModalHeader
            toggle={this.toggle(14)}
            className="text-lg text-blue-700 uppercase font-bold"
          >
            Tambah Realisasi
          </MDBModalHeader>

          <div className="p-5">
            <form
              className="flex flex-col justify-center w-full p-2"
              onSubmit={this.tambahPAD}
            >
              <div className="w-full">
                <label className="text-sm m-0 p-0 text-blue-700">
                  TANGGAL:
                </label>
                <input
                  type="date"
                  name="tanggal"
                  className="focus:bg-green-100 flex-1 block w-full ring-2 p-3 focus:border-blue-700 border mb-2"
                  placeholder="Masukan Nominal Target"
                  onChange={this.ganti}
                  required={true}
                />
              </div>

              <div className="w-full">
                <label className="text-sm m-0 p-0 text-blue-700">
                  TANGGAL STS:
                </label>
                <input
                  type="date"
                  name="tanggal_sts"
                  className="focus:bg-green-100 flex-1 block w-full ring-2 p-3 focus:border-blue-700 border mb-2"
                  placeholder="Masukan Nominal Target"
                  onChange={this.ganti}
                  required={true}
                />
              </div>
              <div className="w-full">
                <label className="text-sm m-0 p-0 text-blue-700">TOTAL:</label>
                <input
                  type="number"
                  name="total"
                  className="focus:bg-green-100 flex-1 block w-full ring-2 p-3 focus:border-blue-700 border mb-3"
                  placeholder="Masukan Nominal Target"
                  onChange={this.ganti}
                  required={true}
                />
              </div>
              <div className="w-full">
                <label className="text-sm m-0 p-0 text-blue-700">FILE:</label>
                <input
                  type="file"
                  name="sts_file"
                  ref="sts_file"
                  className="focus:bg-green-100 flex-1 block w-full ring-2 p-3 focus:border-blue-700 border mb-3"
                  placeholder="Masukan Nominal Total"
                  onChange={(e) => {
                    if (e.target.files[0].type !== "application/pdf") {
                      Notif("e", "❌", "File harus berextensi .pdf");
                      this.refs.sts_file.value = "";
                    } else {
                      this.setState({
                        sts_file: e.target.files[0].name,
                        file: e.target.files[0],
                      });
                    }
                  }}
                  accept={"application/pdf, .pdf"}
                  required={true}
                />
              </div>

              <div className="w-full flex justify-center mt-5">{Tombol}</div>
            </form>
          </div>

          <div className=" text-red-400 p-1 text-center border-t">
            Note: form ini digunakan untuk menambahkan realisasi
          </div>
        </MDBModal>
      </>
    );
  }
}

export default StoreModal;
