import React, { Component } from "react";
import {
  MDBBtn,
  MDBModal,
  MDBModalBody,
  MDBModalHeader,
  MDBModalFooter,
  MDBIcon,
  MDBSpinner,
} from "mdbreact";
import Notif from "../../components/Notif";
import Api from "../../services/Api";
class EditModal extends Component {
  state = {
    modal14: false,
  };

  toggle = (nr) => () => {
    let modalNumber = "modal" + nr;
    this.setState({
      [modalNumber]: !this.state[modalNumber],
      id: this.props.data.id,
      pad_id: this.props.data.pad_id,
      total: this.props.data.realisasi,
      tanggal: this.props.data.tanggal,
      tanggal_sts: this.props.data.sts_tanggal,
      sts_file: this.props.data.sts_file,
      loading: false,
    });
  };
  TambahTarget = async (e) => {
    e.preventDefault();
    this.setState({
      loading: true,
    });
    if (
      !this.state.pad_id ||
      !this.state.tanggal ||
      !this.state.total ||
      !this.state.sts_file ||
      !this.state.tanggal_sts
    ) {
      Notif("e", "❌", "Form Tidak Boleh Kosong");
      this.setState({
        loading: false,
      });
    } else {
      const config = {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token"),
        },
      };
      let fd = new FormData(); //formdata object
      fd.append("file", this.state.file); //append the values with key, value pair
      await Api.post("public/index.php/api/upload", fd, config)
        .then((d) => {
          if (d) {
            Notif("d", "✔️", "File berhasil diupload");
            this.props.refresh();
            this.setState(
              {
                sts_file: d.data.file,
              },
              async () => {
                await Api.put(
                  "public/index.php/api/realisasi/" + this.state.id,
                  this.state,
                  config
                )
                  .then((d) => {
                    if (d) {
                      Notif("d", "✔️", "Realiasi berhasil diedit");
                      this.props.refresh();
                      this.setState({
                        modal14: false,
                        loading: false,
                      });
                    }
                  })
                  .catch((error) => {
                    Notif("e", "😇", "oww.. Realisasi gagal diedit");
                    this.setState({
                      loading: false,
                    });
                  });
              }
            );
          }
        })
        .catch((error) => {
          Notif("e", "😇", "oww.. File gagal diupload");
        });
    }
  };
  ganti = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };
  render() {
    let Tombol = (
      <button
        className="ring-2 w-full mt-5 rounded p-3 bg-blue-700 text-white uppercase focus:ring-4 focus:bg-yellow-500"
        onClick={this.TambahTarget}
      >
        Edit
      </button>
    );
    if (this.state.loading) {
      Tombol = <MDBSpinner />;
    }
    return (
      <>
        <button
          type="button"
          onClick={this.toggle(14)}
          className="m-1 absolute text-blue-500 rounded transition duration-300 hover:bg-yellow-200 p-1 right-1 top-1"
        >
          <MDBIcon icon="pen" />
        </button>

        <MDBModal isOpen={this.state.modal14} toggle={this.toggle(14)} centered>
          <MDBModalHeader
            toggle={this.toggle(14)}
            className="text-lg text-blue-700 uppercase font-bold"
          >
            Edit Realisasi
          </MDBModalHeader>

          <div className="p-5">
            <form
              className="flex flex-col justify-center w-full p-2"
              onSubmit={this.tambahPAD}
            >
              <div className="w-full">
                <label className="text-sm m-0 p-0 text-blue-700">
                  TANGGAL:
                </label>
                <input
                  type="date"
                  name="tanggal"
                  className="focus:bg-green-100 flex-1 block w-full ring-2 p-3 focus:border-blue-700 border mb-2"
                  placeholder="Masukan Nominal Target"
                  onChange={this.ganti}
                  required={true}
                  value={this.state.tanggal}
                />
              </div>

              <div className="w-full">
                <label className="text-sm m-0 p-0 text-blue-700">
                  TANGGAL:
                </label>
                <input
                  type="date"
                  name="tanggal_sts"
                  className="focus:bg-green-100 flex-1 block w-full ring-2 p-3 focus:border-blue-700 border mb-2"
                  placeholder="Masukan Nominal Target"
                  onChange={this.ganti}
                  required={true}
                  value={this.state.tanggal_sts}
                />
              </div>
              <div className="w-full">
                <label className="text-sm m-0 p-0 text-blue-700">TOTAL:</label>
                <input
                  type="number"
                  name="total"
                  value={this.state.total}
                  className="focus:bg-green-100 flex-1 block w-full ring-2 p-3 focus:border-blue-700 border mb-3"
                  placeholder="Masukan Nominal Target"
                  onChange={this.ganti}
                  required={true}
                />
              </div>
              <div className="w-full">
                <label className="text-sm m-0 p-0 text-blue-700">FILE:</label>
                <input
                  type="file"
                  name="sts_file"
                  ref="sts_file"
                  className="focus:bg-green-100 flex-1 block w-full ring-2 p-3 focus:border-blue-700 border mb-3"
                  placeholder="Masukan Nominal Total"
                  onChange={(e) => {
                    if (e.target.files[0].type !== "application/pdf") {
                      Notif("e", "X", "File Harus Berextensi .PDF");
                      this.refs.sts_file.value = "";
                    } else {
                      this.setState({
                        sts_file: e.target.files[0].name,
                        file: e.target.files[0],
                      });
                    }
                  }}
                  accept={"application/pdf, .pdf"}
                  required={true}
                />
              </div>
              <div className="w-full flex justify-center mt-5">{Tombol}</div>
            </form>
          </div>

          <div className=" text-red-400 p-1 text-center border-t">
            Note: form ini digunakan untuk mengubah realisasi
          </div>
        </MDBModal>
      </>
    );
  }
}

export default EditModal;
