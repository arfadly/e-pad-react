import React, { Component } from "react";
import { MDBSpinner } from "mdbreact";
import { Link } from "react-router-dom";

export default class Bulanan extends Component {
  render() {
    const numberFormat = (value) =>
      new Intl.NumberFormat("id-ID", {
        style: "currency",
        currency: "IDR",
      }).format(value);
    const data = this.props.data;
    const data_pad = this.props.data_pad;
    const key = this.props.key;
    const Bulan = (b) => {
      if (b === 1) {
        return "Januari";
      }
      if (b === 2) {
        return "Februari";
      }
      if (b === 3) {
        return "Maret";
      }
      if (b === 4) {
        return "April";
      }
      if (b === 5) {
        return "Mei";
      }
      if (b === 6) {
        return "Juni";
      }
      if (b === 7) {
        return "Juli";
      }
      if (b === 8) {
        return "Agustus";
      }
      if (b === 9) {
        return "September";
      }
      if (b === 10) {
        return "Oktober";
      }
      if (b === 11) {
        return "November";
      }
      if (b === 12) {
        return "Desember";
      }
    };
    let Warna = this.props.warna;

    return (
      <Link
        className="lg:w-1/4 xs:w-full"
        to={{
          pathname: "/realisasi/bulanan/detail",
          state: {
            data: data,
            data_pad: data_pad,
          },
        }}
        key={key}
      >
        <div className="m-4">
          <div className="col-span-12 sm:col-span-6 md:col-span-3">
            <div className="flex flex-row bg-white shadow-sm rounded p-4">
              <div
                className={
                  "flex items-center justify-center flex-shrink-0 h-15 w-20 rounded-xl bg-" +
                  Warna +
                  "-100 font-bold text-" +
                  Warna +
                  "-500"
                }
              >
                {data.capaian_persen}%
              </div>
              <div className="flex flex-col flex-grow ml-4">
                <div className={"text-sm text-" + Warna + "-500"}>
                  Bulan {Bulan(data.bulan)}
                </div>
                <div className="font-bold text-lg">
                  {numberFormat(data.realisasi)}
                </div>
              </div>
            </div>
          </div>
        </div>
      </Link>
    );
  }
}
