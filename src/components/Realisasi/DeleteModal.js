import React, { Component } from "react";
import {
  MDBBtn,
  MDBModal,
  MDBModalBody,
  MDBModalHeader,
  MDBModalFooter,
  MDBIcon,
  MDBSpinner,
} from "mdbreact";
import Notif from "../../components/Notif";
import Api from "../../services/Api";
class DeleteModal extends Component {
  state = {
    modal14: false,
  };

  toggle = (nr) => () => {
    let modalNumber = "modal" + nr;
    this.setState({
      [modalNumber]: !this.state[modalNumber],
      id: this.props.data.id,
      pad_id: this.props.data.pad_id,
      total: this.props.data.realisasi,
      tanggal: this.props.data.tanggal,
      tanggal_sts: this.props.data.sts_tanggal,
      sts_file: this.props.data.sts_file,
      loading: false,
    });
  };
  TambahTarget = async (e) => {
    e.preventDefault();
    this.setState({
      loading: true,
    });
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    };
    await Api.delete("public/index.php/api/realisasi/" + this.state.id, config)
      .then((d) => {
        if (d) {
          Notif("e", "✔️", "realisasi berhasil dihapus");
          this.props.refresh();
          this.setState({
            modal14: false,
            loading: false,
          });
        }
      })
      .catch((error) => {
        Notif("e", "😇", "oww.. ada gagal dirubah");
        this.setState({
          loading: false,
        });
      });
  };
  ganti = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };
  render() {
    let Tombol = (
      <button
        className="ring-2 w-full mt-5 rounded p-3 bg-red-700 text-white uppercase focus:ring-4 focus:bg-yellow-500"
        onClick={this.TambahTarget}
      >
        Hapus
      </button>
    );
    if (this.state.loading) {
      Tombol = <MDBSpinner />;
    }
    return (
      <>
        <button
          type="button"
          onClick={this.toggle(14)}
          className="m-1 absolute text-red-500 rounded transition duration-300 hover:bg-yellow-200 p-1 right-1 top-8"
        >
          <MDBIcon icon="trash" />
        </button>

        <MDBModal isOpen={this.state.modal14} toggle={this.toggle(14)} centered>
          <MDBModalHeader
            toggle={this.toggle(14)}
            className="text-lg text-red-700 uppercase font-bold"
          >
            Hapus Realisasi
          </MDBModalHeader>

          <div className="p-5">
            <form
              className="flex flex-col justify-center w-full p-2"
              onSubmit={this.tambahPAD}
            >
              <div className="text-lg text-center">
                Hapus Realisasi tanggal
                <span className="font-bold text-red-700">
                  {"  " + this.state.tanggal + "  "}
                </span>
                ??
              </div>
              <div className="w-full flex justify-center mt-5">{Tombol}</div>
            </form>
          </div>

          <div className=" text-red-400 p-1 text-center border-t">
            Note: form ini digunakan untuk menghapus realisasi
          </div>
        </MDBModal>
      </>
    );
  }
}

export default DeleteModal;
