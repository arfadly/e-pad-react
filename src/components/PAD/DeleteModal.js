import React, { Component } from "react";
import {
  MDBBtn,
  MDBModal,
  MDBModalBody,
  MDBModalHeader,
  MDBModalFooter,
  MDBIcon,
  MDBSpinner,
} from "mdbreact";
import Notif from "../../components/Notif";
import Api from "../../services/Api";
class DeleteModal extends Component {
  state = {
    modal14: false,
  };

  toggle = (nr) => () => {
    let modalNumber = "modal" + nr;
    this.setState({
      [modalNumber]: !this.state[modalNumber],
      loading: false,
    });
  };
  HapusPAD = async (e) => {
    e.preventDefault();
    this.setState({
      loading: true,
    });
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    };
    await Api.delete("public/index.php/api/pad/" + this.props.data.id, config)
      .then((d) => {
        if (d) {
          Notif("d", "✔️", "Data PAD Berhasil dihapus");
          this.props.refresh();
          this.setState({
            modal14: false,
            loading: false,
          });
        }
      })
      .catch((error) => {
        Notif("e", "😇", "oww.. ada kesalahan");
      });
  };
  ganti = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };
  render() {
    let Tombol = (
      <button
        className="ring-2 w-full mt-5 rounded p-3 bg-red-700 text-white uppercase focus:ring-4 focus:bg-yellow-500"
        onClick={this.HapusPAD}
      >
        Hapus
      </button>
    );
    if (this.state.loading) {
      Tombol = <MDBSpinner />;
    }
    return (
      <>
        <button
          type="button"
          onClick={this.toggle(14)}
          className="px-5 py-2 m-1 border-red-500 border text-red-500 rounded transition duration-300 hover:bg-red-700 hover:text-white focus:outline-none"
        >
          <MDBIcon icon="trash" />
        </button>
        <MDBModal isOpen={this.state.modal14} toggle={this.toggle(14)} centered>
          <MDBModalHeader
            toggle={this.toggle(14)}
            className="text-lg text-blue-700 uppercase font-bold"
          >
            Hapus PAD
          </MDBModalHeader>

          <div className="p-5">
            <div className="text-lg">
              Apakah Yakin Akan Menghapus Nama PAD{" "}
              <strong className="font-extrabold">
                {this.props.data.nama_pad}
              </strong>
              ?
            </div>
            <div className="w-full flex justify-center mt-5">{Tombol}</div>
          </div>

          <div className=" text-red-400 p-1 text-center border-t">
            Note: form ini digunakan untuk mengedit jenis PAD
          </div>
        </MDBModal>
      </>
    );
  }
}

export default DeleteModal;
