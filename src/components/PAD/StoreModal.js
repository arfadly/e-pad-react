import React, { Component } from "react";
import {
  MDBBtn,
  MDBModal,
  MDBModalBody,
  MDBModalHeader,
  MDBModalFooter,
  MDBIcon,
  MDBSpinner,
} from "mdbreact";
import Notif from "../../components/Notif";
import Api from "../../services/Api";
class StoreModal extends Component {
  state = {
    modal14: false,
  };

  toggle = (nr) => () => {
    let modalNumber = "modal" + nr;
    this.setState({
      [modalNumber]: !this.state[modalNumber],
      jenis_pad: 1,
      loading: false,
    });
  };
  tambahPAD = async (e) => {
    e.preventDefault();
    this.setState({
      loading: true,
    });
    const config = {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    };
    await Api.post("public/index.php/api/pad", this.state, config)
      .then((d) => {
        if (d) {
          Notif("d", "✔️", "Tambah PAD Berhasil");
          this.props.refresh();
          this.setState({
            modal14: false,
            loading: false,
          });
        }
      })
      .catch((error) => {
        Notif("e", "😇", "oww.. ada kesalahan");
      });
  };
  ganti = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };
  render() {
    let Tombol = (
      <button
        className="ring-2 w-full mt-5 rounded p-3 bg-blue-700 text-white uppercase focus:ring-4 focus:bg-yellow-500"
        onClick={this.tambahPAD}
      >
        Tambah PAD
      </button>
    );
    if (this.state.loading) {
      Tombol = <MDBSpinner />;
    }
    return (
      <>
        <div className="absolute right-0">
          <button
            type="button"
            className="bg-green-600 hover:bg-green-300 text-white flex-1 focus:ring-2 p-2  m-2 lg:w-20 ml-2 xs:w-10 top-0 "
            onClick={this.toggle(14)}
          >
            <MDBIcon icon="plus" />
          </button>
        </div>
        <MDBModal isOpen={this.state.modal14} toggle={this.toggle(14)} centered>
          <MDBModalHeader
            toggle={this.toggle(14)}
            className="text-lg text-blue-700 uppercase font-bold"
          >
            Tambah PAD
          </MDBModalHeader>

          <div className="p-5">
            <form
              className="flex flex-col justify-center w-full p-2"
              onSubmit={this.tambahPAD}
            >
              <input
                type="text"
                name="nama_pad"
                className="focus:bg-green-100 flex-1 block w-full ring-2 p-3 focus:border-blue-700 border mt-3"
                placeholder="Masukan Nama PAD"
                onChange={this.ganti}
                required={true}
              />
              <div className="w-full  mt-3">
                <div className="text-blue-700 uppercase font-bold">
                  Pilih Jenis PAD
                </div>
                <select
                  name="jenis_pad"
                  className="ring border-none w-full p-3"
                  onChange={this.ganti}
                  defaultValue="1"
                >
                  <option className="bg-blue-300 text-lg" value="1">
                    Retribusi
                  </option>
                  <option className="bg-blue-300 text-lg" value="2">
                    Pajak
                  </option>
                  <option className="bg-blue-300 text-lg" value="3">
                    Lainnya
                  </option>
                </select>
              </div>
              <div className="w-full flex justify-center mt-5">{Tombol}</div>
            </form>
          </div>

          <div className=" text-red-400 p-1 text-center border-t">
            Note: form ini digunakan untuk menambah jenis PAD
          </div>
        </MDBModal>
      </>
    );
  }
}

export default StoreModal;
