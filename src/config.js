let WS_URL = null;
if (process.env.NODE_ENV === "development") {
  WS_URL = "https://pad.kotamobagukota.go.id/api/"; //WEBSERVICE URL
} else {
  WS_URL = "https://pad.kotamobagukota.go.id/api/"; //WEBSERVICE URL
}

export { WS_URL };
